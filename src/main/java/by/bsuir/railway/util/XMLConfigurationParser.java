package by.bsuir.railway.util;

import by.bsuir.railway.model.Connection;
import by.bsuir.railway.model.RailwayNode;
import by.bsuir.railway.model.RailwayNode.RailwayNodeType;
import by.bsuir.railway.model.RailwaySchema;
import by.bsuir.railway.model.Train;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class XMLConfigurationParser {

    private XMLConfigurationParser() {

    }

    public static RailwaySchema parseRailwayXMLSchema(String fileName)
            throws ParserConfigurationException, SAXException, IOException {
        RailwaySchema schema = new RailwaySchema();
        Document doc = prepareDocument(fileName);
        for (RailwayNode node : getRailwayNodes(doc)) {
            schema.addNode(node);
        }
        NodeList nList = doc.getElementsByTagName("Connection");
        for (int index = 0; index < nList.getLength(); index++) {
            Node nNode = nList.item(index);
            Connection connection = new Connection();
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                connection.setOrigin(getNodeByName(schema.getNodes(),
                        eElement.getElementsByTagName("origin").item(0).getTextContent()));
                connection.setDestination(getNodeByName(schema.getNodes(),
                        eElement.getElementsByTagName("destination").item(0).getTextContent()));
            }
            schema.addConnection(connection);
        }
        return schema;
    }

    public static Train parseTrainConfiguration(String fileName) throws IOException, SAXException, ParserConfigurationException {
        Train train = new Train();
        Document doc = prepareDocument(fileName);
        train.setPermanentSpeed(Double.valueOf(doc.getElementsByTagName("speed").item(0).getTextContent()));
        train.setTrainPath(getRailwayNodes(doc));
        train.setX(train.getTrainPath().get(0).getX());
        train.setY(train.getTrainPath().get(0).getY());
        return train;
    }

    private static Document prepareDocument(String fileName) throws ParserConfigurationException, SAXException, IOException {
        File fXmlFile = new File(fileName);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlFile);
        doc.getDocumentElement().normalize();
        return doc;
    }

    private static List<RailwayNode> getRailwayNodes(Document doc) {
        NodeList nList = doc.getElementsByTagName("RailwayNode");
        List<RailwayNode> railwayNodeList = new LinkedList<>();
        for (int index = 0; index < nList.getLength(); index++) {
            Node nNode = nList.item(index);
            RailwayNode railwayNode = new RailwayNode();
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                railwayNode.setName(eElement.getElementsByTagName("name").item(0).getTextContent());
                if (eElement.getElementsByTagName("type").item(0).getTextContent().equals("station")) {
                    railwayNode.setType(RailwayNodeType.STATION);
                } else {
                    railwayNode.setType(RailwayNodeType.SEMAPHORE);
                }
                railwayNode.setX(Float.valueOf(eElement.getElementsByTagName("x").item(0).getTextContent()));
                railwayNode.setY(Float.valueOf(eElement.getElementsByTagName("y").item(0).getTextContent()));
            }
            railwayNodeList.add(railwayNode);
        }
        return railwayNodeList;
    }

    private static RailwayNode getNodeByName(List<RailwayNode> nodes, String name) {
        for (RailwayNode node : nodes) {
            if (node.getName().equals(name)) {
                return node;
            }
        }
        return null;
    }
}
