package by.bsuir.railway.util;

import org.apache.log4j.Logger;
import org.lwjgl.BufferUtils;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import static org.lwjgl.BufferUtils.createByteBuffer;

public class IOUtil {

    private static final Logger logger = Logger.getLogger("serverLogger");

    public static ByteBuffer loadResource(String resource, int bufferSize) {
        try {
            return ioResourceToByteBuffer(resource, bufferSize);
        } catch (IOException e) {
            logger.error("Can't load IO resource", e);
            return null;
        }
    }

    public static ByteBuffer ioResourceToByteBuffer(String resource, int bufferSize) throws IOException {
        ByteBuffer buffer;

        File file = new File(resource);
        if (file.exists() && file.isFile()) {
            FileInputStream source = new FileInputStream(file);
            buffer = ioResourceToByteBuffer(source, bufferSize);
            buffer.flip();
        } else {
            InputStream source = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
            if (source == null) throw new FileNotFoundException(resource);
            buffer = ioResourceToByteBuffer(source, bufferSize);
        }

        return buffer;
    }

    public static ByteBuffer ioResourceToByteBuffer(InputStream resource, int bufferSize) throws IOException {
        ByteBuffer buffer = createByteBuffer(bufferSize);
        if (resource == null) throw new IOException("Input stream resource is null!");
        buffer = readToBuffer(buffer, resource);
        buffer.flip();
        return buffer;
    }

    private static ByteBuffer readToBuffer(ByteBuffer buffer, InputStream source) {
        try (ReadableByteChannel rbc = Channels.newChannel(source)) {
            while (true) {
                int bytes = rbc.read(buffer);
                if (bytes == -1) break;
                if (buffer.remaining() == 0) buffer = resizeBuffer(buffer, buffer.capacity() * 2);
            }
        } catch (IOException e) {
            logger.error("IO can't read to buffer", e);
        }
        return buffer;
    }


    private static ByteBuffer resizeBuffer(ByteBuffer buffer, int newCapacity) {
        ByteBuffer newBuffer = BufferUtils.createByteBuffer(newCapacity);
        buffer.flip();
        newBuffer.put(buffer);
        return newBuffer;
    }
}
