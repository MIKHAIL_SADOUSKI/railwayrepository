package by.bsuir.railway.util;

import by.bsuir.railway.model.RailwayNode;
import by.bsuir.railway.model.Train;
import org.apache.log4j.Logger;
import org.joml.Vector2d;

public final class CalculationUtil {

    private static final Logger logger = Logger.getLogger("clientLogger");

    private static final int RAILWAY_NODE_RADIUS = 10;

    private CalculationUtil() {

    }

    public static void updateTrainCoordinates(Train train) {
        logger.debug("Train coordinates before update: " + train.getX() + " " + train.getY());
        if (!isTrainArrivedToRailwayNode(train, train.getFromNode(), train.getToNode())) {
            double x0 = train.getX();
            double y0 = train.getY();
            double x1 = train.getFromNode().getX();
            double y1 = train.getFromNode().getY();
            double x2 = train.getToNode().getX();
            double y2 = train.getToNode().getY();
            double distance = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
            double calculatedDeltaX = Math.abs(train.getSpeed() * (x2 - x1) / distance);
            logger.debug("New calculated delta x = " + calculatedDeltaX);
            double calculatedDeltaY = Math.abs(train.getSpeed() * (y2 - y1) / distance);
            logger.debug("New calculated delta y = " + calculatedDeltaY);
            if (x2 > x1) {
                train.setX(x0 + calculatedDeltaX);
            } else {
                train.setX(x0 - calculatedDeltaX);
            }
            if (y2 > y1) {
                train.setY(y0 + calculatedDeltaY);
            } else {
                train.setY(y0 - calculatedDeltaY);
            }
        } else {
            logger.debug("Train is arrived to next station. Updating To and From nodes");
            train.updateToAndFromNodes();
            logger.debug("Train after To and From nodes updating " + train);
        }
        logger.debug("Train coordinates after update: " + train.getX() + " " + train.getY());
    }

    public static boolean isTrainArrivedToRailwayNode(Train train, RailwayNode fromNode, RailwayNode toNode) {
        Vector2d fromStationVector = new Vector2d(fromNode.getX(), fromNode.getY());
        Vector2d toStationVector = new Vector2d(toNode.getX(), toNode.getY());
        Vector2d trainVector = new Vector2d(train.getX(), train.getY());

        Vector2d crossStationVector = new Vector2d(toStationVector).sub(fromStationVector);
        Vector2d toNextStationVector = new Vector2d(toStationVector).sub(trainVector);

        Vector2d crossStationVectorNor = new Vector2d(crossStationVector).normalize();
        Vector2d toNextStationVectorNor = new Vector2d(toNextStationVector).normalize();

        Vector2d sum = new Vector2d(crossStationVectorNor).add(toNextStationVectorNor);
        // in this case vectors looks in different directions
        // and that means that train arrived to station.
        // so we need to get path which train already
        // runs over the station coordinates and add it to next path.
        //
        // but now we can't do this so we just set current position to station coordinates.

        // 0.1 that's our threshold
        // length of sum of two normalized vectors should be or zero or 2
        if (sum.length() <= 0.1) {
            train.setX(train.getToNode().getX());
            train.setY(train.getToNode().getY());
            return true;
        }
        return false;
    }

    public static boolean isTrainInToOrFromRailwayNode(Train train) {
        return isTrainArrivedToRailwayNode(train, train.getFromNode(), train.getToNode()) ||
                isTrainInFromStation(train);
    }

    public static double calculateDistance(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    }

    public static double getArriveTime(Train train, RailwayNode nodeToArrive) {
        double distance = calculateDistance(train.getX(), train.getY(), nodeToArrive.getX(), nodeToArrive.getY());
        return distance / train.getPermanentSpeed();
    }

	public static boolean isSameWayTrainsWillNotCrash(Train trainOnTheWay, Train slowerTrainOnStation) {
		double secondArriveTime = getArriveTime(slowerTrainOnStation, slowerTrainOnStation.getToNode());
		if ((slowerTrainOnStation.getPermanentSpeed() - trainOnTheWay.getPermanentSpeed())
				* secondArriveTime > calculateDistance(trainOnTheWay.getX(), trainOnTheWay.getY(),
						slowerTrainOnStation.getX(), slowerTrainOnStation.getY())) {
			return false;
		}
		return true;
	}

    public static boolean isTrainInFromStation(Train train) {
        double trainX = train.getX();
        double trainY = train.getY();
        RailwayNode fromNode = train.getFromNode();
        double squaredDistance = (trainX - fromNode.getX()) * (trainX - fromNode.getX())
                + (trainY - fromNode.getY()) * (trainY - fromNode.getY());
        return squaredDistance <= RAILWAY_NODE_RADIUS * RAILWAY_NODE_RADIUS;
    }
}
