package by.bsuir.railway.client;

import by.bsuir.railway.model.Train;
import by.bsuir.railway.util.CalculationUtil;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.Queue;

public class Engine implements Runnable {

	private static final Logger logger = Logger.getLogger("clientLogger");

	private static final String STOP_TASK = "STOP";
	private static final String RUN_TASK = "RUN";
	private static final double MS_PER_UPDATE = 1000 / 60;
	private static final int MS_FOR_SLEEP = 1;

	private Queue<String> tasks;
	private Train train;
	private Sender sender;

	public Engine() {
		tasks = new LinkedList<>();
	}

	@Override
	public void run() {
		double previous = System.currentTimeMillis();
		double lag = 0.0;
		while (ClientRunningConfiguration.getInstance().isRunning()) {
			double current = System.currentTimeMillis();
			double elapsed = current - previous;
			previous = current;
			lag += elapsed;

			executeTask();

			if (lag >= MS_PER_UPDATE) {
				do {
					CalculationUtil.updateTrainCoordinates(train);
					sender.send(train);
					lag -= MS_PER_UPDATE;
				} while (ClientRunningConfiguration.getInstance().isRunning() && lag >= MS_PER_UPDATE);
			} else {
				// sleep to not load the CPU
				sleep();
			}
		}
	}

	private void sleep() {
		try {
			Thread.sleep(MS_FOR_SLEEP);
		} catch (InterruptedException e) {
			logger.error("The client thread was interrupted", e);
		}
	}

	private void executeTask() {
		String task = tasks.poll();
		if (STOP_TASK.equals(task)) {
			train.setSpeed(0);
		} else if (RUN_TASK.equals(task)) {
			train.setSpeed(train.getPermanentSpeed());
			logger.debug("engine updated train speed to " + train.getSpeed());
		}
	}

	public void addTask(String task) {
		if (task != null) {
			tasks.add(task);
		}
	}

	public Train getTrain() {
		return train;
	}

	public void setTrain(Train train) {
		this.train = train;
	}

	public Sender getSender() {
		return sender;
	}

	public void setSender(Sender sender) {
		this.sender = sender;
	}
}
