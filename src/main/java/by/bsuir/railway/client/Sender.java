package by.bsuir.railway.client;

import by.bsuir.railway.controller.ClientNetworkSender;
import by.bsuir.railway.model.Train;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class Sender implements ClientNetworkSender {

    private static final Logger logger = Logger.getLogger("clientLogger");

    private ObjectOutputStream objectOutputStream;
    private OutputStream out;

    public Sender(OutputStream outputStream) {
        this.out = outputStream;
        try {
            this.objectOutputStream = new ObjectOutputStream(out);
        } catch (IOException e) {
            logger.error("Can't open client object output stream", e);
        }

    }

    @Override
    public void send(Train train) {
        try {
            objectOutputStream.writeObject(train);
            logger.debug("Train was sent to server: " + train);
            objectOutputStream.flush();
            objectOutputStream.reset();
        } catch (IOException e) {
            logger.error("Can't send train state to server. Disconnected from server");
        }
    }

}
