package by.bsuir.railway.client;

import by.bsuir.railway.controller.ClientNetworkReceiver;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Receiver implements Runnable, ClientNetworkReceiver {

    private static final Logger logger = Logger.getLogger("clientLogger");

    private BufferedReader reader;
    private InputStream in;
    private Engine engine;

    public Receiver(InputStream inputStream, Engine engine) {
        this.in = inputStream;
        this.engine = engine;
        this.reader = new BufferedReader(new InputStreamReader(in));
    }

    @Override
    public void run() {
        while (ClientRunningConfiguration.getInstance().isRunning()) {
            try {
                receiveMessage();
            } catch (IOException e) {
                logger.info("Can't receive message from server. Server is unavailabale. Please, try to connect later");
                ClientRunningConfiguration.getInstance().setRunning(false);
            }
        }
    }

    @Override
    public void receiveMessage() throws IOException {
        String serverMessage = reader.readLine();
        logger.debug("Message from server received: " + serverMessage);
        engine.addTask(serverMessage);
    }

}
