package by.bsuir.railway.client;

public final class ClientRunningConfiguration {

    private boolean running;

    private ClientRunningConfiguration() {
        running = true;
    }

    public static class ClientRunningConfigurationHolder {
        public static final ClientRunningConfiguration HOLDER_INSTANCE = new ClientRunningConfiguration();
    }

    public static ClientRunningConfiguration getInstance() {
        return ClientRunningConfigurationHolder.HOLDER_INSTANCE;
    }

    public synchronized boolean isRunning() {
        return running;
    }

    public synchronized void setRunning(boolean running) {
        this.running = running;
    }

}
