package by.bsuir.railway.controller;

public interface ServerNetworkSender {

    public void send(String command);

}
