package by.bsuir.railway.controller;

import java.io.IOException;

public interface ClientNetworkReceiver {

    public void receiveMessage() throws IOException;

}
