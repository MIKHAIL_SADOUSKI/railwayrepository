package by.bsuir.railway.controller;

import by.bsuir.railway.model.Train;

public interface ClientNetworkSender {

    public void send(Train train);

}
