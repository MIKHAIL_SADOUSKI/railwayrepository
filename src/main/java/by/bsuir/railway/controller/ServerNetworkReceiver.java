package by.bsuir.railway.controller;

import by.bsuir.railway.model.Train;

import java.io.IOException;

public interface ServerNetworkReceiver {

    public Train receive() throws IOException, ClassNotFoundException;

}
