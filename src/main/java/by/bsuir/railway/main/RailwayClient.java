package by.bsuir.railway.main;

import by.bsuir.railway.client.Engine;
import by.bsuir.railway.client.Receiver;
import by.bsuir.railway.client.Sender;
import by.bsuir.railway.util.XMLConfigurationParser;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;

public class RailwayClient {

	private static final Logger logger = Logger.getLogger("clientLogger");

	private static final String DEFAULT_HOST = "localhost";
	private static final int DEFAULT_PORT = 3456;
	public static final String DEFAULT_TRAIN_CONFIGURATION_FILE = "TrainConfiguration1.xml";

	public static void main(String[] args) {
		String trainConfigurationFile = (args.length != 0 && args[0] != null) ? args[0]
				: DEFAULT_TRAIN_CONFIGURATION_FILE;
		String host = (args.length > 1 && args[1] != null) ? args[1] : DEFAULT_HOST;
		try {
			int port = (args.length > 2 && args[2] != null) ? Integer.valueOf(args[2]) : DEFAULT_PORT;
			Socket client = new Socket(host, port);
			logger.info("Client connected to server (HOST, PORT): " + host + " " + port);
			Sender sender = new Sender(client.getOutputStream());
			Engine engine = new Engine();
			engine.setTrain(XMLConfigurationParser.parseTrainConfiguration(trainConfigurationFile));
			engine.setSender(sender);
			new Thread(engine, "[ENGINE]").start();
			new Thread(new Receiver(client.getInputStream(), engine), "[RECEIVER]").start();
		} catch (NumberFormatException e) {
			logger.error("Can't convert specified port into integer value", e);
		} catch (FileNotFoundException e) {
			logger.error("The train configuration file wasn't found in path specified " + trainConfigurationFile);
		} catch (IOException e) {
			logger.error("Can't connect to server", e);
		} catch (SAXException | ParserConfigurationException e) {
			logger.error("Can't read train configuration. Please, correct it", e);
		} catch (RuntimeException e) {
			logger.error("Runtime exception has occured", e);
		}
	}
}
