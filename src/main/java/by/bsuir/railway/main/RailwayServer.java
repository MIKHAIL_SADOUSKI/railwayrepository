package by.bsuir.railway.main;

import by.bsuir.railway.model.RailwaySchema;
import by.bsuir.railway.model.RailwayState;
import by.bsuir.railway.server.RailwayClientProcessor;
import by.bsuir.railway.server.ServerRunningConfiguration;
import by.bsuir.railway.server.TrainManager;
import by.bsuir.railway.util.XMLConfigurationParser;
import by.bsuir.railway.server.RailwayView;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class RailwayServer {
	private final static Logger logger = Logger.getLogger("serverLogger");

	private static final int DEFAULT_PORT = 3456;
	private static final int HEIGHT = 600;
	private static final int WIDTH = 800;
	private static final String RAILWAY_SCHEMA_FILENAME = "Railway.xml";
	public static final int SOCKET_TIMEOUT = 1000;

	public static void main(String[] args) {
		String railwayConfigurationFile = (args.length != 0 && args[0] != null) ? args[0] : RAILWAY_SCHEMA_FILENAME;
		Socket client = null;
		try {
			int port = (args.length > 1 && args[1] != null) ? Integer.valueOf(args[1]) : DEFAULT_PORT;
			ServerSocket server = new ServerSocket(port);
			server.setSoTimeout(SOCKET_TIMEOUT);
			RailwayState railwayState = new RailwayState();
			RailwaySchema railwaySchema = XMLConfigurationParser.parseRailwayXMLSchema(railwayConfigurationFile);
			railwayState.setRailwaySchema(railwaySchema);
			RailwayView view = new RailwayView(WIDTH, HEIGHT, railwayState);
			new Thread(view, "[RAILWAY_VIEW]").start();
			TrainManager trainManager = new TrainManager(railwayState);
			new Thread(trainManager, "[TRAIN_MANAGER]").start();
			while (ServerRunningConfiguration.getInstance().isRunning()) {
				try {
					client = server.accept();
					logger.info("New client connected");
					RailwayClientProcessor clientProcessor = new RailwayClientProcessor(client, railwayState);
					new Thread(clientProcessor, "[CLIENT_PROCESSOR]").start();
					trainManager.addClientProcessor(clientProcessor);
				} catch (IOException e) {
					logger.info("No more clients connected yet");
				}
			}
		} catch (NumberFormatException e) {
			logger.error("Can't convert specified port into integer value", e);
		} catch (FileNotFoundException e) {
			logger.error("The railway configuration file wasn't found in path specified " + railwayConfigurationFile);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			logger.error("Can't read railway schema configuration. Please correct it", e);
		} catch (RuntimeException e) {
			logger.error("Runtime exception has occured", e);
		}
	}
}
