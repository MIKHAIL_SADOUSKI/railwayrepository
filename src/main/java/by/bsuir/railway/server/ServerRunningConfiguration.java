package by.bsuir.railway.server;

public final class ServerRunningConfiguration {
    private boolean running;

    private ServerRunningConfiguration() {
        running = true;
    }

    public static class ServerRunningConfigurationHolder {
        public static final ServerRunningConfiguration HOLDER_INSTANCE = new ServerRunningConfiguration();
    }

    public static ServerRunningConfiguration getInstance() {
        return ServerRunningConfigurationHolder.HOLDER_INSTANCE;
    }

    public synchronized boolean isRunning() {
        return running;
    }

    public synchronized void setRunning(boolean running) {
        this.running = running;
    }
}
