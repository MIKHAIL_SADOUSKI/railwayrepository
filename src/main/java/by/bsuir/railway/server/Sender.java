package by.bsuir.railway.server;

import by.bsuir.railway.controller.ServerNetworkSender;
import org.apache.log4j.Logger;

import java.io.OutputStream;
import java.io.PrintWriter;

public class Sender implements ServerNetworkSender {

    private static final Logger logger = Logger.getLogger("serverLogger");

    private PrintWriter writer;
    private OutputStream out;
    private boolean sendMessage;

    public Sender(OutputStream outputStream) {
        this.sendMessage = true;
        this.out = outputStream;
        this.writer = new PrintWriter(out, true);
    }

    @Override
    public void send(String command) {
        if (sendMessage) {
            writer.println(command);
            logger.debug("send message to client " + command);
        }
    }

    public void stopSendMessages() {
        sendMessage = false;
    }
}
