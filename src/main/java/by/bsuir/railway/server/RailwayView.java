package by.bsuir.railway.server;

import by.bsuir.railway.model.Connection;
import by.bsuir.railway.model.RailwayNode;
import by.bsuir.railway.model.RailwayNode.RailwayNodeType;
import by.bsuir.railway.model.RailwayState;
import by.bsuir.railway.model.Train;
import by.bsuir.railway.util.IOUtil;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.nanovg.NVGColor;
import org.lwjgl.nanovg.NanoVGGL3;
import org.lwjgl.opengl.GL;

import java.nio.ByteBuffer;
import java.util.List;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.nanovg.NanoVG.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.*;

public class RailwayView implements Runnable {
    public static final String FONT_NAME = "FONT_NAME";
    public static final ByteBuffer FONT_DATA = IOUtil.loadResource("by/bsuir/railway/server/font/Roboto-Regular.ttf",
            1024);
    private int width;
    private int height;
    private long window;
    private long nvgContext;
    private int[] ww = new int[]{0};
    private int[] hh = new int[]{0};

    private NVGColor lineColor;
    private NVGColor stationNodeColor;
    private NVGColor semaphoreNodeColor;
    private NVGColor trainColor;
    private RailwayState railwayState;

    private int tSize = 10;

    public RailwayView(int width, int height, RailwayState railwayState) {
        this.width = width;
        this.height = height;
        this.railwayState = railwayState;
    }

    public void run() {
        initialize();
        while (ServerRunningConfiguration.getInstance().isRunning()) {
            glClearColor(1, 1, 1, 1);
            glfwGetWindowSize(window, ww, hh);
            // Set viewport size
            glViewport(0, 0, width = ww[0], height = hh[0]);
            // Clear screen
            glClear(GL_COLOR_BUFFER_BIT);

            render();

            // poll events to callbacks
            glfwPollEvents();
            glfwSwapBuffers(window);
        }
        destroy();
    }

    public void stop() {
        ServerRunningConfiguration.getInstance().setRunning(false);
    }

    private void initialize() {
        System.setProperty("joml.nounsafe", Boolean.TRUE.toString());
        System.setProperty("java.awt.headless", Boolean.TRUE.toString());
        if (!GLFW.glfwInit()) {
            throw new RuntimeException("Can't initialize GLFW");
        }
        window = glfwCreateWindow(width, height, "Railway", NULL, NULL);
        GLFWVidMode glfwVidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        glfwSetWindowPos(window, (glfwVidMode.width() - this.width) / 2, (glfwVidMode.height() - this.height) / 2);
        glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
            if (key == GLFW.GLFW_KEY_ESCAPE && action != GLFW_RELEASE)
                ServerRunningConfiguration.getInstance().setRunning(false);
        });
        glfwSetWindowCloseCallback(window, window -> ServerRunningConfiguration.getInstance().setRunning(false));

        glfwMakeContextCurrent(window);
        GL.createCapabilities();
        glfwSwapInterval(0);

        glfwShowWindow(window);
        nvgContext = NanoVGGL3.nvgCreate(NanoVGGL3.NVG_STENCIL_STROKES | NanoVGGL3.NVG_ANTIALIAS);
        nvgCreateFontMem(nvgContext, FONT_NAME, FONT_DATA, 0);

        lineColor = NVGColor.calloc();
        lineColor.r(0);
        lineColor.g(0);
        lineColor.b(0);
        lineColor.a(1);

        stationNodeColor = NVGColor.calloc();
        stationNodeColor.r(0.9f);
        stationNodeColor.g(0.9f);
        stationNodeColor.b(0.7f);
        stationNodeColor.a(1);

        semaphoreNodeColor = NVGColor.calloc();
        semaphoreNodeColor.r(0.5f);
        semaphoreNodeColor.g(0.5f);
        semaphoreNodeColor.b(0.5f);
        semaphoreNodeColor.a(1);

        trainColor = NVGColor.calloc();
        trainColor.r(0.7f);
        trainColor.g(0.9f);
        trainColor.b(0.7f);
        trainColor.a(1);
    }

    private void destroy() {
        NanoVGGL3.nvgDelete(nvgContext);
        glfwDestroyWindow(window);
        glfwTerminate();
    }

    private void render() {
        glDisable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        nvgBeginFrame(nvgContext, width, height, 1);

        renderRailwaySchema();

        nvgEndFrame(nvgContext);
        glDisable(GL_BLEND);
        glEnable(GL_DEPTH_TEST);
    }

    private void renderRailwaySchema() {
        renderLines(railwayState.getRailwaySchema().getConnections());
        renderNodes(railwayState.getRailwaySchema().getNodes());
        renderTrains(railwayState.getTrains());
    }

    private void renderLines(List<Connection> connections) {
        for (Connection connection : connections) {
            renderLine(connection.getOrigin().getX(), connection.getOrigin().getY(), connection.getDestination().getX(),
                    connection.getDestination().getY());
        }
    }

    private void renderNodes(List<RailwayNode> nodes) {
        for (RailwayNode railwayNode : nodes) {
            if (RailwayNodeType.SEMAPHORE.equals(railwayNode.getType())) {
                renderNode(railwayNode.getX(), railwayNode.getY(), 10f, semaphoreNodeColor);
                renderStationName(railwayNode.getX() + 20, railwayNode.getY(), railwayNode.getName());
            } else {
                renderNode(railwayNode.getX(), railwayNode.getY(), 10f, stationNodeColor);
                renderStationName(railwayNode.getX() + 30, railwayNode.getY(), railwayNode.getName());
            }
        }
    }

    private void renderTrains(List<Train> trains) {
        for (Train train : trains) {
            renderTrain(train.getX(), train.getY());
        }
    }

    private void renderStationName(double x, double y, String text) {
        ByteBuffer byteText = null;
        try {
            byteText = memUTF8(text, false);
            long start = memAddress(byteText);
            long end = start + byteText.remaining();
            nvgFontSize(nvgContext, 20);
            nvgFontFace(nvgContext, FONT_NAME);
            nvgTextAlign(nvgContext, NVG_ALIGN_LEFT | NVG_ALIGN_MIDDLE);
            nvgBeginPath(nvgContext);
            nvgFillColor(nvgContext, lineColor);
            nnvgText(nvgContext, (float) x, (float) y, start, end);
        } finally {
            if (byteText != null) {
                memFree(byteText);
            }
        }
    }

    private void renderLine(double x1, double y1, double x2, double y2) {
        nvgLineCap(nvgContext, NVG_ROUND);
        nvgLineJoin(nvgContext, NVG_ROUND);
        nvgStrokeWidth(nvgContext, 1f);
        nvgStrokeColor(nvgContext, lineColor);
        nvgBeginPath(nvgContext);
        nvgMoveTo(nvgContext, (float) x1, (float) y1);
        nvgLineTo(nvgContext, (float) x2, (float) y2);
        nvgStroke(nvgContext);
    }

    private void renderNode(double x, double y, double r, NVGColor nodeColor) {
        nvgBeginPath(nvgContext);
        nvgStrokeColor(nvgContext, lineColor);
        nvgFillColor(nvgContext, nodeColor);
        nvgCircle(nvgContext, (float) x, (float) y, (float) r);
        nvgFill(nvgContext);
        nvgStroke(nvgContext);
    }

    private void renderTrain(double x, double y) {
        nvgBeginPath(nvgContext);
        nvgRect(nvgContext, (float) x - tSize / 2f, (float) y - tSize / 2f, tSize, tSize);
        nvgStrokeColor(nvgContext, lineColor);
        nvgFillColor(nvgContext, trainColor);
        nvgFill(nvgContext);
        nvgStroke(nvgContext);
    }

}
