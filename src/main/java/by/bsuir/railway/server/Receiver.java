package by.bsuir.railway.server;

import by.bsuir.railway.controller.ServerNetworkReceiver;
import by.bsuir.railway.model.Train;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

public class Receiver implements Runnable, ServerNetworkReceiver {

    private static final Logger logger = Logger.getLogger("serverLogger");

    private ObjectInputStream objectInputStream;
    private InputStream in;
    private RailwayClientProcessor processor;
    private boolean receiveMessages;

    public Receiver(InputStream inputStream, RailwayClientProcessor processor) {
        this.receiveMessages = true;
        this.in = inputStream;
        this.processor = processor;
        try {
            this.objectInputStream = new ObjectInputStream(in);
        } catch (IOException e) {
            logger.error("Can't open server object input stream", e);
        }
    }

    @Override
    public void run() {
        while (ServerRunningConfiguration.getInstance().isRunning() && receiveMessages) {
            try {
                Train train = receive();
                logger.debug("Train received from Client " + train);
                if (processor.getTrainId() == null) {
                    processor.setTrainId(train.getId());
                    processor.getRailwayState().addTrain(train);
                } else if (processor.getTrainId().equals(train.getId())) {
                    processor.getRailwayState().updateTrain(train);
                    logger.debug("Railway State updated with new train state " + processor.getRailwayState().getTrains().get(0));
                }
            } catch (IOException | ClassNotFoundException e) {
                logger.info("Can't read Train object on server. Client with trainId = " + processor.getTrainId() + " was disconnected.");
                int trainIndex = processor.getRailwayState().findTrainIndexById(processor.getTrainId());
                if (trainIndex != -1) {
                    processor.getRailwayState().getTrains().remove(trainIndex);
                }
                receiveMessages = false;
                processor.stopSendMessages();
            }
        }
    }

    @Override
    public Train receive() throws IOException, ClassNotFoundException {
        return (Train) objectInputStream.readObject();
    }
}
