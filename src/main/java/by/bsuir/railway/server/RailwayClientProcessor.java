package by.bsuir.railway.server;

import by.bsuir.railway.model.RailwayState;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.Socket;

public class RailwayClientProcessor implements Runnable {

    private static final Logger logger = Logger.getLogger("serverLogger");

    private Socket client;
    private RailwayState state;
    private Sender sender;
    private String trainId;

    public RailwayClientProcessor (Socket client, RailwayState state) {
        this.client = client;
        this.state = state;
    }

    @Override
    public void run() {
        try {
            new Thread(new Receiver(client.getInputStream(), this), "[SERVER_RECEIVER]").start();
            sender = new Sender(client.getOutputStream());
        } catch (IOException e) {
            logger.error("Can't get client output stream on server side", e);
        }
    }

    public Sender getSender() {
        return sender;
    }

    public RailwayState getRailwayState() {
        return state;
    }

    public String getTrainId() {
        return trainId;
    }

    public void setTrainId(String trainId) {
        this.trainId = trainId;
    }

    public void stopSendMessages() {
        this.sender.stopSendMessages();
    }
}
