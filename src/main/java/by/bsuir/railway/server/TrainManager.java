package by.bsuir.railway.server;

import by.bsuir.railway.model.Connection;
import by.bsuir.railway.model.RailwayState;
import by.bsuir.railway.model.Train;
import by.bsuir.railway.util.CalculationUtil;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TrainManager implements Runnable {

	private static final Logger logger = Logger.getLogger("serverLogger");

	private static final String STOP_COMMAND = "STOP";
	private static final String RUN_COMMAND = "RUN";
	private static final double MS_PER_UPDATE = 100;
	private static final long MS_FOR_SLEEP = 1;

	private RailwayState railwayState;
	private List<RailwayClientProcessor> clientProcessors;

	public TrainManager(RailwayState railwayState) {
		this.railwayState = railwayState;
		this.clientProcessors = new ArrayList<>();
	}

	@Override
	public void run() {
		double previous = System.currentTimeMillis();
		double lag = 0.0;
		while (ServerRunningConfiguration.getInstance().isRunning()) {
			double current = System.currentTimeMillis();
			double elapsed = current - previous;
			previous = current;
			lag += elapsed;

			if (lag >= MS_PER_UPDATE) {
				do {
					manageTrains();
					lag -= MS_PER_UPDATE;
				} while (ServerRunningConfiguration.getInstance().isRunning() && lag >= MS_PER_UPDATE);
			} else {
				// sleep to not load the CPU
				sleep();
			}
		}
	}

	private void sleep() {
		try {
			Thread.sleep(MS_FOR_SLEEP);
		} catch (InterruptedException e) {
			logger.error("The client thread was interrupted", e);
		}
	}

	public void addClientProcessor(RailwayClientProcessor processor) {
		if (processor != null) {
			clientProcessors.add(processor);
		}
	}

	private RailwayClientProcessor getClientProcessorById(String id) {
		for (RailwayClientProcessor clientProcessor : clientProcessors) {
			if (clientProcessor.getTrainId() != null && clientProcessor.getTrainId().equals(id)) {
				return clientProcessor;
			}
		}
		return null;
	}

	private void sendCommand(String command, List<String> trainIds) {
		for (String trainId : trainIds) {
			if (trainId != null && command != null) {
				getClientProcessorById(trainId).getSender().send(command);
			}
		}
	}

	private void manageTrains() {
		List<Train> trainsOnStation = new ArrayList<>();
		List<Train> trainsOnTheWay = new ArrayList<>();
		List<String> stopTrainIds = new ArrayList<>();
		List<String> startTrainIds = new ArrayList<>();

		populateTrainsOnTheWayAndOnStation(trainsOnStation, trainsOnTheWay);

		// stop trains on station when train on the way will arrive to this
		// station
		stopTrainsOnStationIfOtherArrives(trainsOnStation, trainsOnTheWay, stopTrainIds);

		// if trains on station want to go to station of each other - check
		// quicker and run it, stop others
		manageTrainsThatWantToArriveToEachOther(trainsOnStation, stopTrainIds, startTrainIds);

		// if some train on the way and train on station wants to to go to the
		// same station - check whether
		// they will not crash with slower train and start it, stop others
		manageTrainsThatCanCatchUpAnother(trainsOnStation, trainsOnTheWay, stopTrainIds, startTrainIds);

		// if trains from one station want to go to same station - choose
		// quicker and run it, stop others
		manageTrainsThatDepartFromSameStation(trainsOnStation, stopTrainIds, startTrainIds);

		// run train in other case
		for (Train train : trainsOnStation) {
			if (train.getSpeed() == 0.0) {
				logger.debug("send run to train " + train);
				startTrainIds.add(train.getId());
			}
		}

		sendCommand(STOP_COMMAND, stopTrainIds);
		sendCommand(RUN_COMMAND, startTrainIds);
	}

	private void manageTrainsThatDepartFromSameStation(List<Train> trainsOnStation, List<String> stopTrainIds,
			List<String> startTrainIds) {
		Map<Connection, List<Train>> connectionTrainMap = getConnectionTrainsMap(trainsOnStation);
		List<Train> toRemove = new ArrayList<>();
		for (Map.Entry<Connection, List<Train>> entry : connectionTrainMap.entrySet()) {
			Train quickerTrain = getQuickerTrain(entry.getValue());
			startTrainIds.add(quickerTrain.getId());
			toRemove.add(quickerTrain);
			for (Train train : entry.getValue()) {
				if (!train.equals(quickerTrain)) {
					stopTrainIds.add(train.getId());
					toRemove.add(train);
				}
			}
		}
		trainsOnStation.removeAll(toRemove);
	}

	private void manageTrainsThatCanCatchUpAnother(List<Train> trainsOnStation, List<Train> trainsOnTheWay,
			List<String> stopTrainIds, List<String> startTrainIds) {
		Map<Connection, List<Train>> connectionTrainsOnStationMap = getConnectionTrainsMap(trainsOnStation);
		Map<Connection, List<Train>> connectionTrainsOnTheWayMap = getConnectionTrainsMap(trainsOnTheWay);

		List<Train> toRemove = new ArrayList<>();
		for (Map.Entry<Connection, List<Train>> entry : connectionTrainsOnStationMap.entrySet()) {
			if (connectionTrainsOnTheWayMap.containsKey(entry.getKey())) {
				Train slowerTrain = getSlowerTrain(entry.getValue());
				Train trainWithBiggerDistance = getTrainWithBiggestDistanceToArrive(
						connectionTrainsOnTheWayMap.get(entry.getKey()));
				if (CalculationUtil.isSameWayTrainsWillNotCrash(trainWithBiggerDistance, slowerTrain)) {
					startTrainIds.add(slowerTrain.getId());
				} else {
					stopTrainIds.add(slowerTrain.getId());
				}
				toRemove.add(slowerTrain);
				for (Train train : entry.getValue()) {
					if (!train.equals(slowerTrain)) {
						stopTrainIds.add(train.getId());
						toRemove.add(train);
					}
				}
			}
		}
		trainsOnStation.removeAll(toRemove);
	}

	private void manageTrainsThatWantToArriveToEachOther(List<Train> trainsOnStation, List<String> stopTrainIds,
			List<String> startTrainIds) {
		List<Connection> busyConnections = new ArrayList<>();
		List<Train> toRemove = new ArrayList<>();
		for (Train train : trainsOnStation) {
			for (Train anotherTrainOnStation : trainsOnStation) {
				if (!train.getId().equals(anotherTrainOnStation.getId())
						&& train.getToNode().equals(anotherTrainOnStation.getFromNode())
						&& train.getFromNode().equals(anotherTrainOnStation.getToNode())) {
					Connection connection = new Connection(train.getFromNode(), train.getToNode());
					if (busyConnections.contains(connection)) {
						stopTrainIds.add(anotherTrainOnStation.getId());
						stopTrainIds.add(train.getId());
						toRemove.add(train);
						toRemove.add(anotherTrainOnStation);
					} else {
						Train quickerTrain = getQuickerTrain(Arrays.asList(train, anotherTrainOnStation));
						startTrainIds.add(quickerTrain.getId());
						if (train.equals(quickerTrain)) {
							stopTrainIds.add(anotherTrainOnStation.getId());
						} else {
							startTrainIds.add(train.getId());
						}
						toRemove.add(train);
						toRemove.add(anotherTrainOnStation);
						busyConnections.add(new Connection(train.getFromNode(), train.getToNode()));
						busyConnections.add(new Connection(train.getToNode(), train.getFromNode()));
					}
				}
			}
		}
		trainsOnStation.removeAll(toRemove);
	}

	private void stopTrainsOnStationIfOtherArrives(List<Train> trainsOnStation, List<Train> trainsOnTheWay,
			List<String> stopTrainIds) {
		List<Train> toRemove = new ArrayList<>();
		for (Train train : trainsOnTheWay) {
			for (Train trainOnStation : trainsOnStation) {
				if (train.getToNode().equals(trainOnStation.getFromNode())
						&& train.getFromNode().equals(trainOnStation.getToNode())) {
					logger.debug("stop train as another train is arriving " + trainOnStation);
					stopTrainIds.add(trainOnStation.getId());
					toRemove.add(trainOnStation);
				}
			}
		}
		trainsOnStation.removeAll(toRemove);
	}

	private void populateTrainsOnTheWayAndOnStation(List<Train> trainsOnStation, List<Train> trainsOnTheWay) {
		for (Train train : railwayState.getTrains()) {
			if (CalculationUtil.isTrainInToOrFromRailwayNode(train)) {
				logger.debug("add to train on station " + train);
				trainsOnStation.add(train);
			} else {
				logger.debug("add to train on the way " + train);
				trainsOnTheWay.add(train);
			}
		}
	}

	private Map<Connection, List<Train>> getConnectionTrainsMap(List<Train> trainsOnStation) {
		Map<Connection, List<Train>> connectionTrainMap = new HashMap<>();
		for (Train train : trainsOnStation) {
			Connection connection = new Connection(train.getFromNode(), train.getToNode());
			if (!connectionTrainMap.containsKey(connection)) {
				List<Train> trainsOnConnection = new ArrayList<>();
				trainsOnConnection.add(train);
				connectionTrainMap.put(connection, trainsOnConnection);
			} else {
				connectionTrainMap.get(connection).add(train);
			}
		}
		return connectionTrainMap;
	}

	private Train getQuickerTrain(List<Train> trains) {
		Train quickerTrain = null;
		double maxSpeed = 0.0;
		for (Train train : trains) {
			if (train.getPermanentSpeed() > maxSpeed) {
				maxSpeed = train.getPermanentSpeed();
				quickerTrain = train;
			}
		}
		return quickerTrain;
	}

	private Train getSlowerTrain(List<Train> trains) {
		Train slowerTrain = null;
		double minSpeed = Double.MAX_VALUE;
		for (Train train : trains) {
			if (train.getPermanentSpeed() < minSpeed) {
				minSpeed = train.getPermanentSpeed();
				slowerTrain = train;
			}
		}
		return slowerTrain;
	}

	private Train getTrainWithBiggestDistanceToArrive(List<Train> trains) {
		Train trainWithBiggerDistance = null;
		double maxDistance = 0.0;
		for (Train train : trains) {
			double trainDistance = CalculationUtil.calculateDistance(train.getX(), train.getY(),
					train.getToNode().getX(), train.getToNode().getY());
			if (trainDistance > maxDistance) {
				maxDistance = trainDistance;
				trainWithBiggerDistance = train;
			}
		}
		return trainWithBiggerDistance;
	}
}
