package by.bsuir.railway.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RailwaySchema {

    private List<Connection> connections;
    private List<RailwayNode> nodes;

    public RailwaySchema() {
        this.connections = new ArrayList<>();
        this.nodes = new ArrayList<>();
    }

    public List<Connection> getConnections() {
        return Collections.unmodifiableList(connections);
    }

    public List<RailwayNode> getNodes() {
        return Collections.unmodifiableList(nodes);
    }

    public void addConnection(Connection connection) {
        if (connection != null) {
            connections.add(connection);
        }
    }

    public void addNode(RailwayNode node) {
        if (node != null) {
            nodes.add(node);
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((connections == null) ? 0 : connections.hashCode());
        result = prime * result + ((nodes == null) ? 0 : nodes.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RailwaySchema other = (RailwaySchema) obj;
        if (connections == null) {
            if (other.connections != null)
                return false;
        } else if (!connections.equals(other.connections))
            return false;
        if (nodes == null) {
            if (other.nodes != null)
                return false;
        } else if (!nodes.equals(other.nodes))
            return false;
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("RailwaySchema [connections=");
        builder.append(connections);
        builder.append(", nodes=");
        builder.append(nodes);
        builder.append("]");
        return builder.toString();
    }

}
