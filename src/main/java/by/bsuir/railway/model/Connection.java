package by.bsuir.railway.model;

public class Connection {

    private RailwayNode origin;
    private RailwayNode destination;
    
    public Connection () {
    	
    }
    
    public Connection (RailwayNode origin, RailwayNode destination) {
    	this.destination = destination;
    	this.origin = origin;
    }

    public RailwayNode getOrigin() {
        return origin;
    }

    public void setOrigin(RailwayNode origin) {
        this.origin = origin;
    }

    public RailwayNode getDestination() {
        return destination;
    }

    public void setDestination(RailwayNode destination) {
        this.destination = destination;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((destination == null) ? 0 : destination.hashCode());
        result = prime * result + ((origin == null) ? 0 : origin.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Connection other = (Connection) obj;
        if (destination == null) {
            if (other.destination != null)
                return false;
        } else if (!destination.equals(other.destination))
            return false;
        if (origin == null) {
            if (other.origin != null)
                return false;
        } else if (!origin.equals(other.origin))
            return false;
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Connection [origin=");
        builder.append(origin);
        builder.append(", destination=");
        builder.append(destination);
        builder.append("]");
        return builder.toString();
    }

}
