package by.bsuir.railway.model;

import java.util.ArrayList;
import java.util.List;

public class RailwayState {

    private RailwaySchema railwaySchema;
    private List<Train> trains;

    public RailwayState() {
        trains = new ArrayList<>();
    }

    public RailwaySchema getRailwaySchema() {
        return railwaySchema;
    }

    public void setRailwaySchema(RailwaySchema railwaySchema) {
        this.railwaySchema = railwaySchema;
    }

    public void addTrain(Train train) {
        if (train != null) {
            trains.add(train);
        }
    }

    public void updateTrain(Train train) {
        if (train != null) {
            int trainIndex = findTrainIndexById(train.getId());
            if (trainIndex != -1) {
                trains.set(trainIndex, train);
            }
        }
    }

    public List<Train> getTrains() {
        return trains;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("RailwayState [railwaySchema=");
        builder.append(railwaySchema);
        builder.append(", trains=");
        builder.append(trains);
        builder.append("]");
        return builder.toString();
    }

    public int findTrainIndexById(String id) {
        for (int trainIndex = 0; trainIndex < trains.size(); trainIndex++) {
            if (trains.get(trainIndex).getId().equals(id)) {
                return trainIndex;
            }
        }
        return -1;
    }

}
