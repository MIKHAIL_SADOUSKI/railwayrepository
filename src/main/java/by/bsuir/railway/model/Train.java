package by.bsuir.railway.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class Train implements Serializable {

    private static final long serialVersionUID = -664315174628105473L;

    private transient List<RailwayNode> trainPath;
    private String id;
    private RailwayNode fromNode;
    private RailwayNode toNode;
    private double x;
    private double y;
    private double currentSpeed;
    private double permanentSpeed;

    public Train() {
        id = UUID.randomUUID().toString();
    }

    public double getSpeed() {
        return currentSpeed;
    }

    public void setSpeed(double speed) {
        this.currentSpeed = speed;
    }

    public double getPermanentSpeed() {
        return permanentSpeed;
    }

    public void setPermanentSpeed(double permanentSpeed) {
        this.permanentSpeed = permanentSpeed;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public List<RailwayNode> getTrainPath() {
        return trainPath;
    }

    public void setTrainPath(List<RailwayNode> trainPath) {
        this.trainPath = new LinkedList<>(trainPath);
        setToAndFromNodes();
    }

    private void setToAndFromNodes() {
        if (this.trainPath.size() == 1) {
            toNode = fromNode = this.trainPath.get(0);
        } else {
            fromNode = this.trainPath.get(0);
            toNode = this.trainPath.get(1);
        }
    }

    public void updateToAndFromNodes() {
        int toNodeIndex = trainPath.indexOf(toNode);
        if (trainPath.indexOf(toNode) == trainPath.size() - 1) {
            Collections.reverse(trainPath);
            setToAndFromNodes();
        } else if (toNodeIndex + 1 <= trainPath.size() - 1) {
            fromNode = trainPath.get(toNodeIndex);
            toNode = trainPath.get(toNodeIndex + 1);
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RailwayNode getFromNode() {
        return fromNode;
    }

    public void setFromNode(RailwayNode fromNode) {
        this.fromNode = fromNode;
    }

    public RailwayNode getToNode() {
        return toNode;
    }

    public void setToNode(RailwayNode toNode) {
        this.toNode = toNode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Train train = (Train) o;

        if (Double.compare(train.x, x) != 0) return false;
        if (Double.compare(train.y, y) != 0) return false;
        if (Double.compare(train.currentSpeed, currentSpeed) != 0) return false;
        if (Double.compare(train.permanentSpeed, permanentSpeed) != 0) return false;
        if (!id.equals(train.id)) return false;
        if (fromNode != null ? !fromNode.equals(train.fromNode) : train.fromNode != null) return false;
        return toNode != null ? toNode.equals(train.toNode) : train.toNode == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id.hashCode();
        result = 31 * result + (fromNode != null ? fromNode.hashCode() : 0);
        result = 31 * result + (toNode != null ? toNode.hashCode() : 0);
        temp = Double.doubleToLongBits(x);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(currentSpeed);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(permanentSpeed);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Train [currentSpeed=");
        builder.append(currentSpeed);
        builder.append(", permanentSpeed=");
        builder.append(permanentSpeed);
        builder.append(", trainPath=");
        builder.append(trainPath);
        builder.append(", id=");
        builder.append(id);
        builder.append(", fromNode=");
        builder.append(fromNode);
        builder.append(", toNode=");
        builder.append(toNode);
        builder.append(", x=");
        builder.append(x);
        builder.append(", y=");
        builder.append(y);
        builder.append("]");
        return builder.toString();
    }
}
